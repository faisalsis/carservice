import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Car } from '../car';
import { DataService } from '../data.service';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {
  inputForm = new FormGroup({
    vin: new FormControl(''),
    brand : new FormControl(''),
    year: new FormControl(''),
    color: new FormControl(''),
    prices: new FormControl(''),
    type: new FormControl('')
  });
  cars: Car[];
  public actionState = undefined;
  constructor(private ds: DataService) { }

  ngOnInit() {
    this.ds.data2.subscribe($data2 => {
      if ($data2) {
        this.inputForm.patchValue($data2);
        this.actionState = 'update';
      } else {
        this.actionState = 'insert';
        this.inputForm.reset();
      }
    });
  }

  saveCar() {
    if (this.actionState === 'update') {
      this.ds.updCarS(this.inputForm.value);
    } else if (this.actionState === 'insert') {
      this.ds.addCarS(this.inputForm.value);
    }
    this.clear();
  }

  clear() {
    this.actionState = '';
    this.inputForm.reset();
  }
}

export function NoNullValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const isNull = (control.value === null || control.value === undefined || control.value === '' || control.pristine);
    const isValid = !isNull;
    return isValid ? null : { 'NoNull': 'value do not empty' };
  };
}

export function NoPatternValidator(regex = new RegExp('^[0-9]*$')): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
      const text = regex.test(control.value);
      return text ? null : { 'NoPattern': { value: control.value } };
  };
}

