import { Component, OnInit } from '@angular/core';
import { Car } from '../car';
import { DataService } from '../data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  constructor(private ds: DataService) { }
  cols: any[];
  cars: Car[];

  ngOnInit() {
    this.ds.data.subscribe(newData => {
      this.cars = [...newData];
    });
    this.ds.getCarS();
    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' },
      { field: 'prices', header: 'Price' },
      { field: 'type', header: 'Type' }
    ];
  }

  getData(car) {
    this.ds.setData(car);
  }

  delCar(car) {
    this.ds.delCarS(car.vin);
  }

}
