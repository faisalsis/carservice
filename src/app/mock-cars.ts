import { Car } from './car';

export const CARS: Car[] = [
    { vin: 'dsad231ff',	 year: 2012,	brand: 'VW'	 ,      color: 'Orange',    prices: 2700,   type: 'Sedan'},
    { vin: 'gwregre345', year: 2011,	brand: 'Audi',      color: 'Black',     prices: 1200,   type: 'MPV'},
    { vin: 'h354htr',	 year: 2005,	brand: 'Renault',   color: 'Gray',      prices: 3000,   type: 'Convert'},
    { vin: 'j6w54qgh',   year: 2003,	brand: 'BMW',       color: 'Blue',      prices: 2900,   type: 'Sport'},
    { vin: 'hrtwy34',	 year: 1995,	brand: 'Mercedes',  color: 'Orange',    prices: 3300,   type: 'Convert'},
    { vin: 'jejtyj',     year: 2005,	brand: 'Volvo',     color: 'Black',     prices: 2200,   type: 'JEEP'},
    { vin: 'g43gr',	     year: 2012,	brand: 'Honda',     color: 'Yellow',    prices: 1500,   type: 'MPV'},
    { vin: 'greg34',     year: 2013,	brand: 'Jaguar',    color: 'Orange',    prices: 4000,   type: 'Sedan'},
    { vin: 'h54hw5',	 year: 2000,	brand: 'Ford',      color: 'Black',     prices: 3500,   type: 'Sport'},
    { vin: '245t2s',     year: 2013,	brand: 'Fiat',      color: 'Red',       prices: 2500,   type: 'JEEP'}
    ];
